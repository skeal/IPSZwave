<?
    // Klassendefinition
    class IPSZwave extends IPSModule {
 
        // Der Konstruktor des Moduls
        // Überschreibt den Standard Kontruktor von IPS
        public function __construct($InstanceID) {
            // Diese Zeile nicht löschen
            parent::__construct($InstanceID);
 
            // Selbsterstellter Code
        }
 
        // Überschreibt die interne IPS_Create($id) Funktion
        public function Create() {
            // Diese Zeile nicht löschen.
            parent::Create();
            
            $this->RegisterVariableInteger("AnzahlGeraete", "Anzahl der Zwave-Geraete");
 
        }
 
        // Überschreibt die intere IPS_ApplyChanges($id) Funktion
        public function ApplyChanges() {
            // Diese Zeile nicht löschen
            parent::ApplyChanges();
            
            $this->GetZwaveDevices();
        }
 
        /**
        * Die folgenden Funktionen stehen automatisch zur Verfügung, wenn das Modul über die "Module Control" eingefügt wurden.
        * Die Funktionen werden, mit dem selbst eingerichteten Prefix, in PHP und JSON-RPC wiefolgt zur Verfügung gestellt:
        *
        * ABC_MeineErsteEigeneFunktion($id);
        *
        */
        public function Destroy()
        {
            $this->UnregisterTimer("Update_RSS_Feed");
        
            //Never delete this line!!
            parent::Destroy();
        }
        
        public function GetZwaveDevices()
        {
            if($this->GetIDForIdent("AnzahlGeraete") === false)
            {
                $this->RegisterVariableInteger("AnzahlGeraete", "Anzahl der Zwave-Geraete");
            }
        
            $zwaveCount = 0;
            
            $alleInstanzen = IPS_GetInstanceListByModuleType(3);
            
            foreach($alleInstanzen as $singleDevice)
            {
                if (IPS_GetInstance($singleDevice)["ModuleInfo"]["ModuleName"] === "Z-Wave Module")
                {
                    $zwaveCount++;
                    $zwaveDeviceArray[] = $singleDevice;
                }
            }
            $this->SetValueInteger("AnzahlGeraete", $zwaveCount);
            
            foreach($zwaveDeviceArray as $singleZwaveDevice)
            {
                $zwaveIdent = str_replace(' ', '', IPS_GetName($singleZwaveDevice));
                if ($this->GetIDForIdent($zwaveIdent) === false) {
                    $this->RegisterVariableString($zwaveIdent, IPS_GetName($singleZwaveDevice));
                }
            }
        }
        
        private function SetValueInteger($Ident, $Value)
        {
		$id = $this->GetIDforIdent($Ident);
		if (GetValueInteger($id) <> $Value)
		{
			SetValueInteger($id, $Value);
			return true;
		}
		return false;
  	    }
  	    
  	    private function SetValueString($Ident, $Value)
        {
		$id = $this->GetIDforIdent($Ident);
		if (GetValueString($id) <> $Value)
		{
			SetValueString($id, $Value);
			return true;
		}
		return false;
  	    }

    }
?>