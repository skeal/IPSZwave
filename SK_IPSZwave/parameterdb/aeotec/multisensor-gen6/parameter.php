// ParameterID / Length / Default / Description
$parameter = array(
    array(2, 1, 0, "Enable/Disable waking up for 10 minutes when re-power on (battery mode) the MultiSensor"),
    array(3, 2, 240, "1. The default PIR time is 4 minutes. The Multisensor will send BASIC SET CC"),
    array(4, 1, 5, "Enable/disable the function of motion sensor.")
);